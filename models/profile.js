const mongoose = require('mongoose')
const { Schema } = mongoose

const proifleSchema = new Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
    },
    fullName: {
        type: String,
        require: true
    },
    companyName: {
        type: String,
        require: true
    },
    companyId: {
        type: String,
        require: true
    },
	creditBalance: {
		type: Number,
		required: true
	}
})

module.exports = mongoose.model('profile', proifleSchema)
