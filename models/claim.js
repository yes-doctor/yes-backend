const mongoose = require('mongoose')
const { Schema } = mongoose

const claimSchema = new Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	companyName: {
		type: String,
		required: true
	},
	companyId: {
		type: Number,
		required: true
	},
	receiptId: {
		type: Number,
		required: true,
		unique: true
	},
	clinicId: {
		type: Number,
		required: true
	},
	claimType: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true
	}
})

module.exports = mongoose.model('claim', claimSchema)
