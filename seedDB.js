const mongoose = require('mongoose')
const faker = require('faker')
const Claim = require('./models/claim')
const User = require('./models/user')
const Profile = require('./models/profile')
const profileComponent = require('./components/profile')

const generateFakeClaim = (userId, companyId, companyName) => ({
	companyName,
	companyId,
	receiptId: 5000 + Math.ceil(Math.random() * 5000),
	userId,
	clinicId: Math.ceil(Math.random() * 1000),
	claimType: 'CASH',
	amount: 5000
})

const createUser = async function(username, email, password) {
	// create new user
	const user = new User({
		username,
		email,
		password
	})
	// save user
	await user.save()
	return user
}
const createProfile = async function(user) {
	const profile = new Profile({
		userId: user._id,
		fullName: 'David James',
		companyName: faker.company.companyName(),
		companyId: Math.ceil(Math.random() * 1000),
		creditBalance: 100000
	})
	// save user
	await profile.save()
	return profile
}

module.exports.seedDB = async function(databaseURL, numClaim) {
	try {
		console.log('seeding database...')
		await mongoose.connect(databaseURL)
		await User.collection.drop()
		await Profile.collection.drop()
		let user = await createUser('yesdoctor', 'yesdoctor@gmail.com', 'yesdoctor')
		let profile = await createProfile(user)
		if (numClaim > 0) {
			await Claim.collection.drop()
			for (let i = 0; i < numClaim; i++) {
				const newClaim = generateFakeClaim(
					user._id,
					profile.companyId,
					profile.companyName
				)
				const createdClaim = await Claim.create({ ...newClaim })
				await profileComponent.updateProfile(createdClaim)
			}
			console.log(`Successfully created ${numClaim} claims..`)
		}
	} catch (e) {
		console.log(e)
		process.exit()
	}
}
