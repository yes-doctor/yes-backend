const fs = require('fs')
const http = require('http')
const https = require('https')
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const authRouter = require('./routers/auth')
const claimRouter = require('./routers/claim')
const profileRouter = require('./routers/profile')
const mongoose = require('mongoose')
const cors = require('cors')
const ejs = require('ejs')
const passport = require('passport')
const CONFIG = require('./config/config')
const { seedDB } = require('./seedDB')

let environment = CONFIG.environment
const app = express()
// connect to database
const { databaseURL } = CONFIG[environment]
mongoose.connect(databaseURL, err => {
	if (err) {
		console.log(err)
		process.exit()
	}
	console.log(`Successfully connected to ${databaseURL}`)
	seedDB(databaseURL, 3)
})

// allow cross-origin request
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Credentials', true)
	res.header('Access-Control-Allow-Origin', req.headers.origin)
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
	res.header(
		'Access-Control-Allow-Headers',
		'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, token'
	)
	if (req.method === 'OPTIONS') {
		console.log('OPTIONS...')
		res.sendStatus(200)
	} else {
		next()
	}
})

// middlewares
app.use(cors())
app.use(morgan('combined'))
app.use(
	bodyParser.json({
		type: '*/*'
	})
)

authRouter(app)
claimRouter(app)
profileRouter(app)

const httpServer = http.createServer(app)
httpServer.listen(CONFIG.port.http)
console.log(`HTTP Server listening at port ${CONFIG.port.http}`)

if (environment === 'production') {
	const privateKey = fs.readFileSync(
		`${CONFIG.certificateDirectory}/privkey.pem`,
		'utf8'
	)
	const certificate = fs.readFileSync(
		`${CONFIG.certificateDirectory}/cert.pem`,
		'utf8'
	)
	const ca = fs.readFileSync(`${CONFIG.certificateDirectory}/chain.pem`, 'utf8')
	const credentials = {
		key: privateKey,
		cert: certificate,
		ca: ca
	}
	const httpsServer = https.createServer(credentials, app)
	httpsServer.listen(CONFIG.server.https_port, err => {
		if (err) console.log(err)
		console.log(`Https Server listening at ${CONFIG.port.https}`)
	})
}
module.exports = app
