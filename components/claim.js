const Claim = require('../models/claim')
const profileComponent = require('./profile')
const util = require('./util')

exports.createClaim = util.asyncErrorHandlerMiddleware(
	async (req, res, next) => {
		const inputClaim = req.body
		try {
			const createdClaim = await new Claim({ ...inputClaim })
			await createdClaim.save()
			await profileComponent.updateProfile(createdClaim)
			util.writeSuccessResponse(res)
		} catch (e) {
			console.log(e)
			util.writeErrorResponse(res, 500, `Error while trying to create space`)
		}
		next()
	}
)

exports.getAllClaims = util.asyncErrorHandlerMiddleware(
	async (req, res, next) => {
		const claims = await Claim.find({})
		res.response = { claims }
		next()
	}
)

exports.getClaimById = util.asyncErrorHandlerMiddleware(
	async (req, res, next) => {
		const claim = await Claim.findById(req.params.id)
		res.response = { claim }
		next()
	}
)
