const R = require('ramda')

exports.respond = (req, res) => {
	// todo: log or record something somewhere
	res.json(res.response)
}

exports.asyncErrorHandlerMiddleware = fn => (req, res, next) => {
	Promise.resolve(fn(req, res, next)).catch(err => {
		console.log(err)
		return res.status(501).json({
			code: 501,
			error: err.message
		})
	})
}

exports.ensureAuthenticated = (req, res, next) => {
	if (req.isAuthenticated()) {
		return next()
	}
	console.log('Please signin to post...')
	res.status(401).json({
		code: 401,
		message: 'unauthorize'
	})
}

exports.writeSuccessResponse = (res, successMsg) => {
	let responseBody = { success: true }
	if (successMsg) responseBody.message = successMsg

	res.status(200)
	res.response = responseBody
	return res
}

exports.writeErrorResponse = (res, errorCode, errorMsg) => {
	res.status(errorCode)
	res.response = {
		success: false,
		error: errorMsg
	}
	return res
}
