const Profile = require('../models/profile')
const util = require('./util')

exports.createProfile = async data => {
	let { userId, fullName, companyId, companyName } = data
	try {
		const createdProfile = await new Profile({
			userId,
			fullName,
			companyId,
			companyName,
			creditBalance: 100000
		})
		await createdProfile.save()
		return createdProfile
	} catch (e) {
		console.log(e)
	}
}

const calcuateNewCredit = (claim, profile) => {
	let newCredit = profile.creditBalance
	if (claim.claimType === 'CREDIT') newCredit -= claim.amount
	else if (claim.claimType === 'CASH') newCredit += claim.amount
	return newCredit
}

exports.updateProfile = async claim => {
	const profile = await Profile.findOne({
		userId: claim.userId
	})
	if (!profile) {
		console.log('No profile found')
		return
	}
	const newCredit = calcuateNewCredit(claim, profile)
	const updatedProfile = await Profile.findByIdAndUpdate(profile._id, {
		creditBalance: newCredit
	})
}

exports.getProfileByUserId = util.asyncErrorHandlerMiddleware(
	async (req, res, next) => {
		const userId = req.params.userId
		const profile = await Profile.findOne({
			userId
		})
		res.response = { profile }
		next()
	}
)
