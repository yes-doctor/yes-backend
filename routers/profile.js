const { respond, ensureAuthenticated } = require('../components/util')
const profileComponent = require('../components/profile')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', { session: false })

module.exports = app => {
	app.get(
		'/api/profile/:userId',
		requireAuth,
		profileComponent.getProfileByUserId,
		respond
	)
}
