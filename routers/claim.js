const { respond } = require('../components/util')
const claimComponent = require('../components/claim')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', { session: false })

module.exports = app => {
	app.post('/api/claim', requireAuth, claimComponent.createClaim, respond)
	app.get('/api/claim', claimComponent.getAllClaims, respond)

	app.get('/api/claim/:id', claimComponent.getClaimById, respond)
}
